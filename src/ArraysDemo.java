import java.util.Arrays;

public class ArraysDemo {
    public static void init() {
        /* First way to declare an array */
        // int[] numbers = new int[3];
        // numbers[0]= 10;
        // numbers[1] = 20;
        // numbers[2]= 30;

        /* Second way to declare and initiate an array */
        int[] numbers = { 10, 20, 30 };

        System.out.println(Arrays.toString(numbers));
    }

    private int[] items; // = {} (empty array)
    private int count = 0;

    public ArraysDemo(int length) {
        this.items = new int[length];
    }

    public void print() {
        for (int i = 0; i < count; i++) {
            System.out.println(items[i]);
        }
    }

    public void insert(int value) {
        int initialLength = items.length;
        if (count < initialLength) {
            items[count] = value;
        } else {
            int[] newItems = new int[initialLength + 1];

            System.arraycopy(items, 0, newItems, 0, initialLength); // ( original, startPosition in original, dest, start Pos dest, number of items to be copied )

            newItems[count] = value;
            items = newItems;
        }
        count++;

        /**
         * complexity: O(n)
         */
    }

    public void remove() {
        if (items.length == 0)
            return;

        int[] newitems = new int[items.length - 1];
        System.arraycopy(items, 0, newitems, 0, items.length - 1);

        items = newitems;
        count--;

        /**
         * Complexity: O(n)
         */
    }

    public void removeAt(int index) {
        if (index < 0 || index > items.length)
            return;

        int[] newItems = new int[items.length - 1];
        for (int i = 0, j = 0; i < items.length; i++) {
            if (i != index) {
                newItems[j] = items[i];
                j++;
            }
        }

        items = newItems;
        count--;
        /**
         * Complexity: O(n)
         */
    }

    public int indexOf(int value) {
        int result = -1;
        for (int index = 0; index < items.length; index++) {
            if (items[index] == value) {
                result = index;
            }
        }
        return result;

        /**
         * Complexity: O(n)
         */
    }
}


/*
* Other array classes:
*  - Vector (synchronous)
*  - ArrayList
*      ArrayList<Integer> numbers;
* 
* 
*/