import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        arrayDemo();

    }

    
    public static void arrayDemo(){
        //ArraysDemo.init();

        ArraysDemo numbers = new ArraysDemo(3);
        numbers.insert(10);
        numbers.insert(11);
        numbers.insert(12);
        numbers.insert(13);
        numbers.insert(14);
        numbers.removeAt(2);
        numbers.insert((20));
        numbers.removeAt(4);

        numbers.print();

        System.out.println(numbers.indexOf(11));
    }

    public void arrayListDemo(){
        ArrayList<Integer> list = new ArrayList<>();
        list.add(11);
        list.add(12);
        list.add(13);
        list.remove(0);

        list.indexOf(13);
        Object[] array = list.toArray();
        
    }
}
